import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { FormBuilder } from '@angular/forms';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      providers: [
        FormBuilder
      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1#primenumber').textContent).toContain('Welcome to primenumber!');
  }));
  it(`convert string '1' to number`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const input_number = '1';
    const app = fixture.debugElement.componentInstance;
    expect(app.convertStringToNumber(input_number)).toEqual(1);
  }));
  it(`convert string 'string' to number`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const input_number = 'string';
    const app = fixture.debugElement.componentInstance;
    expect(app.convertStringToNumber(input_number)).toEqual(-1);
  }));
  it('check prime number 13', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const component = fixture.componentInstance;
    expect(component['checkPrimeNumber'](13)).toEqual({ status: true, message: '13 is prime number.' });
  }));
  it('check prime number 12', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const component = fixture.componentInstance;
    expect(component['checkPrimeNumber'](12)).toEqual({ status: false, message: `12 isn't prime number.` });
  }));
  it('show message is prime number', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    const data = { status: true, message: 'is prime number' };
    expect(app.showMessage(data)).toContain('is prime number');
  }));
  it('show message not prime number', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    const data = { status: false, message: 'not prime number' };
    expect(app.showMessage(data)).toContain('not prime number');
  }));
  it('validdate submit is true', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    app.formPrimeNumber.controls['value'].setValue(13);
    expect(app.validateSubmit(app.formPrimeNumber)).toEqual(true);
  }));
  it('validdate submit is false', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    app.formPrimeNumber.controls['value'].setValue(null);
    expect(app.validateSubmit(app.formPrimeNumber)).toEqual(false);
  }));
});
