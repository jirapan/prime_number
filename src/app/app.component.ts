import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  public formPrimeNumber: FormGroup;
  constructor(protected fb: FormBuilder) {
    this.formPrimeNumber = this.fb.group({
      value: ['', Validators.required]
    });
  }

  public checkPrimeNumber(number: number): { status: boolean, message: string } {
    for (let i = 2; i < number; i++) {
      if (number % i === 0) {
        return {
          status: false,
          message: `${number} isn't prime number.`
        };
      }
    }
    if (number !== 1) {
      return {
        status: true,
        message: `${number} is prime number.`
      };
    } else {
      return {
        status: false,
        message: `${number} isn't prime number.`
      };
    }
  }

  public convertStringToNumber(value: string) {
    // tslint:disable-next-line:radix
    const num = parseInt(value);
    return num ? num : -1;
  }

  public showMessage(data: { status: boolean, message: string }): string {
    if (data.status) {
      return 'is prime number';
    } else {
      return 'not prime number';
    }
  }

  public validateSubmit(form: FormGroup): boolean {
    return form.valid && (typeof form.value.value === 'number');
  }
}
